package com.nurkhtsay.wastetracker.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Primary = Color(0xFFEADDFF)
val Secondary = Color(0xFFFEF7FF)
val Ternary = Color(0xFF7D5260)

val OnSurface = Color(0xFF49454F)
val BackGround = Color(0xFFE7E0EC)