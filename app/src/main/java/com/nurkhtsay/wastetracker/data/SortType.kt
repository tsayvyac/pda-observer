package com.nurkhtsay.wastetracker.data

enum class SortType {
    NAME,
    LAST_ADDED,
    EXPIRY
}